use std::str::FromStr;

use crate::error::Error;

pub const RANGES: [&str; 11] = ["1d", "5d", "1mo", "3mo", "6mo", "1y", "2y", "5y", "10y", "ytd", "max"];
const INTERVALS: [&str; 6] = ["1m", "2m", "5m", "15m", "60m", "1d"];

/// Valid range for the Yahoo API.
pub struct Range(String);

impl Range {
    pub fn as_str(&self) -> &str {
        self.0.as_str()
    }
}

impl FromStr for Range {
    type Err = Error;

    fn from_str(str: &str) -> Result<Self, Self::Err> {
        if RANGES.contains(&str) {
            Ok(Self(str.to_owned()))
        } else {
            Err(Error::InvalidRange(str.to_owned()))
        }
    }
}

/// Valid interval for the Yahoo API.
pub struct Interval(String);

impl Interval {
    pub fn as_str(&self) -> &str {
        self.0.as_str()
    }
}

impl FromStr for Interval {
    type Err = Error;

    fn from_str(str: &str) -> Result<Self, Self::Err> {
        if INTERVALS.contains(&str) {
            Ok(Self(str.to_owned()))
        } else {
            Err(Error::InvalidInterval(str.to_owned()))
        }
    }
}
