mod error;
mod types;
mod analytics;

use std::process::exit;

use chrono::{NaiveDateTime, DateTime, Utc};
use clap::Clap;
use yahoo_finance_api::{self as yahoo, Quote, YahooError, YahooConnector};

use crate::types::*;
use crate::error::Error;

#[derive(Clap)]
#[clap(version = "0.1")]
struct Opts {
    /// Fetch quotes from this range of dates
    #[clap(short, long, default_value = "1mo", possible_values = &RANGES)]
    from: Range,
    #[clap(required = true)]
    symbols: Vec<String>,
}

fn main() {
    if let Err(error) = run() {
        eprintln!("error: {}", error.to_string());
        exit(1);
    }
}

fn run() -> Result<(), Box<dyn std::error::Error>> {
    let provider = yahoo::YahooConnector::new();
    let opts = Opts::parse();
    let range = opts.from;
    let last_month = "1mo".parse()?;
    let interval = "1d".parse()?;

    let mut wtr = csv::WriterBuilder::new()
        .delimiter(b',')
        .from_writer(std::io::stdout());

    // write headers
    wtr.write_record(&["period start", "symbol", "price", "change %", "min", "max", "30d avg"])?;

    for symbol in opts.symbols.iter() {
        let range_quotes = fetch_quotes(&provider, symbol, &range, &interval)?;
        let last_month_quotes = fetch_quotes(&provider,  symbol, &last_month, &interval)?;

        let range_prices = extract_prices(&range_quotes);
        let min = analytics::min(&range_prices).ok_or(Error::FailedAnalytics("Not enough quotes".to_string()))?;
        let max = analytics::max(&range_prices).ok_or(Error::FailedAnalytics("Not enough quotes".to_string()))?;
        let diffs = analytics::price_diff(&range_prices).ok_or(Error::FailedAnalytics("Not enough quotes".to_string()))?;

        let last_month_prices = extract_prices(&last_month_quotes);
        let last_month_average = last_month_prices.iter().sum::<f64>() / last_month_prices.len() as f64;

        wtr.write_record(&[
            &format_timestamp(range_quotes[range_quotes.len() - 1].timestamp),
            symbol,
            &format!("${:.2}", range_prices[0]),
            &format!("{:.2} %", diffs.1),
            &format!("${:.2}", min),
            &format!("${:.2}", max),
            &format!("${:.2}", last_month_average),
        ])?;
    }
    Ok(())
}

/// Downloads quotes of the given ticker from Yahoo! Finance.
fn fetch_quotes(provider: &YahooConnector, ticker: &str, range: &Range, interval: &Interval) -> Result<Vec<Quote>, YahooError> {
    let response = provider.get_quote_range(ticker, interval.as_str(), range.as_str())?;
    response.quotes()
}

/// Extracts (adjusted closing) prices from quotes.
fn extract_prices(quotes: &[Quote]) -> Vec<f64> {
    quotes.into_iter().map(|quote| quote.adjclose).collect()
}

/// Formats timestamp to RFC 3339
fn format_timestamp(timestamp: u64) -> String {
    DateTime::<Utc>::from_utc(NaiveDateTime::from_timestamp(timestamp as i64, 0), Utc).to_rfc3339()
}
