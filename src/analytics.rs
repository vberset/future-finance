/// Finds the minimum value.
pub fn min(series: &[f64]) -> Option<f64> {
    series.split_first().map(|(init, rest)|
        rest.iter().fold(*init, |acc, item| acc.min(*item))
    )
}

/// Finds the maximum value.
pub fn max(series: &[f64]) -> Option<f64> {
    series.split_first().map(|(init, rest)|
        rest.iter().fold(*init, |acc, item| acc.max(*item))
    )
}

/// Computes the simple moving average over the series.
pub fn n_window_sma(n: usize, series: &[f64]) -> Option<Vec<f64>> {
    Some(series.chunks(n).map(|chunk| chunk.iter().sum::<f64>() / (chunk.len() as f64)).collect())
}

/// Computes price differences (percentage, absolute difference).
pub fn price_diff(series: &[f64]) -> Option<(f64, f64)> {
    let first = series.first()?;
    let last = series.last()?;
    Some((*last / *first, *last - * first))
}

#[cfg(test)]
mod tests {
    use std::num::NonZeroUsize;
    use quickcheck_macros::quickcheck;

    use crate::analytics;

    #[test]
    fn test_min_on_empty() {
        assert_eq!(analytics::min(&[]), None);
    }

    #[quickcheck]
    fn test_min_is_lesser_or_equal(series: Vec<f64>) {
        let series: Vec<_> = series.into_iter().filter(|p| p.is_normal()).collect();
        if let Some(min) = analytics::min(&series) {
            for price in series {
                assert!(min <= price);
            }
        }
    }

    #[test]
    fn test_max_on_empty() {
        assert_eq!(analytics::max(&[]), None);
    }

    #[quickcheck]
    fn test_max_is_greater_or_equal(series: Vec<f64>) {
        let series: Vec<_> = series.into_iter().filter(|p| p.is_normal()).collect();
        if let Some(max) = analytics::max(&series) {
            for price in series {
                assert!(max >= price);
            }
        }
    }

    #[quickcheck]
    fn test_sma_values_count(series: Vec<f64>, n: NonZeroUsize) {
        let n = usize::from(n);
        if let Some(sma) = analytics::n_window_sma(n, &series) {
            if series.len() == 0 {
                assert_eq!(sma.len(), 0);
            } else {
                let expected = series.len() / n +  if series.len() % n == 0 { 0 } else { 1 };
                assert_eq!(sma.len(), expected);
            }
        }
    }
}
