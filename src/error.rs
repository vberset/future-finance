use std::fmt;

#[derive(Debug)]
pub enum Error {
    InvalidInterval(String),
    InvalidRange(String),
    FailedAnalytics(String),
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Error::InvalidInterval(details) => {
                write!(f, "invalid interval: {}", details)
            }
            Error::InvalidRange(details) => {
                write!(f, "invalid range: {}", details)
            }
            Error::FailedAnalytics(details) => {
                write!(f, "failed analytics: {}", details)
            }
        }
    }
}

impl std::error::Error for Error {}
