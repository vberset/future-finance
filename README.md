# Future Finance Labs' Simple Stock Tracking CLI

Quote analysis tool.

## Usage

Simply run `future-finance` with the list of desired stock symbols:
```
$ future-finance MSFT GOOG AAPL UBER IBM
period start,symbol,price,change %,min,max,30d avg
2021-03-08T21:00:01+00:00,MSFT,$243.21,-15.82 %,$226.73,$244.43,$236.51
2021-03-08T21:00:02+00:00,GOOG,$2083.51,-59.34 %,$2024.17,$2128.31,$2079.60
2021-03-08T21:00:02+00:00,AAPL,$136.01,-19.65 %,$116.36,$136.01,$127.26
2021-03-08T21:00:02+00:00,UBER,$59.61,-6.41 %,$51.45,$63.18,$56.75
2021-03-08T21:00:02+00:00,IBM,$122.10,2.71 %,$118.93,$124.81,$121.22
```

You can specify the quotes range with the `--from` flag:
```
$ future-finance --from 1y MSFT GOOG AAPL UBER IBM
```
for one year range.

For a detailed usage, run:
```
$ future-finance --help
```
